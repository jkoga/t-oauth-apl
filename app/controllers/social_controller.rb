class SocialController < ApplicationController
  def new
    logger.debug(request.env['omniauth.auth'])
    @user = User.from_omniauth(request.env['omniauth.auth'])
    unless @user
      redirect_to action: 'oauth_failure'
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      session[:provider] = @user.provider
      session[:uid] = @user.uid
      session[:name] = @user.name
      session[:email] = @user.email
      redirect_to '/home/index'
    else
      render action: 'new'
    end
  end

  def destroy
    reset_session
    redirect_to '/home/index', :notice => 'サインアウトしました'
  end

  def oauth_failure
    redirect_to '/home/index', :notice => 'サインイン処理が中断されました'
  end

  private
  def user_params
    params.require(:user).permit(:provider, :uid, :name, :email, :password)
  end
end
