class User < ActiveRecord::Base


  def self.from_omniauth(auth)
    case auth.provider
      when 'facebook' then
        self.omniauth_facebook(auth)
      when 'google_oauth2' then
        omniauth_google(auth)
      else
        nil
    end
  end

  private
  def self.omniauth_facebook(auth)
    # 認証時のトークンを使ってGraphAPIで日本語情報を取得
    facebook = Koala::Facebook::API.new(auth.credentials.token)
    result = facebook.get_object('/me', { locale: 'ja_JP' })

    user = User.new
    user.provider = auth.provider
    user.uid = result['id']
    user.name = result['name']
    user.email = result['email']
    return user
  end

  private
  def self.omniauth_google(auth)
    user = User.new
    user.provider = auth.provider
    user.uid = auth.uid
    user.name = auth.info.name
    user.email = auth.info.email
    return user
  end
end
